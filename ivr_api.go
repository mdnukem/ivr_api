package main

import (
	"bytes"
	"database/sql"
	"errors"
	"github.com/gin-gonic/gin"
	_ "github.com/mattn/go-sqlite3"
	"github.com/op/go-logging"
	"io/ioutil"
	"ivrsvc"
	"log"
	"net/http"
	_ "net/http/httputil"
	"os"
	"strconv"
)

//************************* Logging **********************************
//********************************************************************

var apiLog = logging.MustGetLogger("apiService")
var logFmt = logging.MustStringFormatter(
	"%{color}%{time:15:04:05.000} %{shortfunc}-> %{level:.5s} %{id:03x}%{color:reset} %{message}")
var logBackend = logging.NewLogBackend(os.Stderr, "", 0)
var fmtr = logging.NewBackendFormatter(logBackend, logFmt)

//************************* Globals :( *******************************
//********************************************************************
var (
	RxtDb *sql.DB
)

//************************* Startup **********************************
//********************************************************************

func main() {
	app := gin.Default()

	var dberr error
	RxtDb, dberr = sql.Open("sqlite3", "./rxterms.db")
	if dberr != nil {
		apiLog.Fatal("Could not open the RxTerms database: " + dberr.Error())
	}
	defer RxtDb.Close()

	dberr = RxtDb.Ping()
	if dberr != nil {
		apiLog.Fatal("Could not ping RxTerms database: " + dberr.Error())
	}

	app.GET("/", Index)

	app.GET("/search/vitamins", GetVitamins)
	app.GET("/search/vitamins/", GetVitamins)

	app.GET("/Patient/:ptid", PatientDetail)

	app.GET("Patient/:ptid/Medications", GetMedications)
	app.GET("Patient/:ptid/Medications/", GetMedications)

	app.POST("Patient/:ptid/Medications/:rxcui/update", PostMedicationUpdate)
	app.POST("Patient/:ptid/Vitamins/update", PostVitaminUpdate)

	apiLog.Info("Starting up the CHIP IVR Service")
	apiLog.Fatal(app.Run(":8001"))
}

//************************* Route Handlers ***************************
//********************************************************************

func Index(c *gin.Context) {
	c.JSON(200, gin.H{"result": "This is the service root"})
}

//Retrieves a patient's demographic information
func PatientDetail(c *gin.Context) {
	_, ptId, err := getPatientIdFromPath(c)
	if err != nil {
		apiLog.Error("%v", err.Error())
		return
	}

	pt, err := getPatientForId(ptId, c)
	if err != nil {
		apiLog.Error("%v", err.Error())
		return
	}

	c.JSON(http.StatusOK, pt)
}

//Retrieves all medications for a given patient
func GetMedications(c *gin.Context) {
	_, ptId, err := getPatientIdFromPath(c)

	resp, err := ivrsvc.PendingMedicationsForPatient(ptId, RxtDb)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"Error": err.Error()})
		return
	} else {
		c.JSON(http.StatusOK, resp)
		return
	}
}

//Returns a list of all vitamins we are interested in.
func GetVitamins(c *gin.Context) {
	vlist, err := ivrsvc.GetVitaminList()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"Error": "There was a problem retrieving possible vitamins"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": vlist})
	return
}

//Updates a specific medication's usage information for a
//patient
func PostMedicationUpdate(c *gin.Context) {
	var update []ivrsvc.MedicationUpdate
	ptIdStr, ptId, err := getPatientIdFromPath(c)
	rxcuiStr := c.Param("rxcui")

	//Get the patient the update pertains to.
	pt, err := getPatientForId(ptId, c)
	if err != nil {
		apiLog.Error("%v", err.Error())
		return
	}

	//Retrieve the body of the update sent to the route
	body, err := ioutil.ReadAll(c.Request.Body)
	if gin.Mode() == gin.DebugMode {
		//dumpPostData(body)
	}
	if err != nil {
		apiLog.Error("%v", err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"Error": err.Error()})
		return
	}

	//Unmarshal the medication update
	err = ivrsvc.UnmarshalIVRMedUpdate(body, &update, RxtDb)
	if err != nil {
		apiLog.Error("%v", err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"Error": err.Error()})
		return
	}

	err = nil
	for i, _ := range update {
		if update[i].Medication.RxCUI == rxcuiStr {
			err = ivrsvc.SendMedicationStatement(&update[i], &pt, ptId)

			log.Printf("Updated Medication:\n%v", &update[i])
			if err != nil {
				break
			}
		}
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError,
			gin.H{"Error": "There was an error saving the updated medication:" + err.Error()})
	} else {
		c.JSON(http.StatusOK, gin.H{"info": "Update was successful for Patient Id: " + ptIdStr})
	}
}

func PostVitaminUpdate(c *gin.Context) {
	_, ptId, err := getPatientIdFromPath(c)
	//Get the patient the update pertains to.
	pt, err := getPatientForId(ptId, c)
	if err != nil {
		apiLog.Error("%v", err.Error())
		return
	}

	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(http.StatusBadRequest,
			gin.H{"Error": "Bad Request"})
		return
	}
	//dumpPostData(body)

	vu := make([]ivrsvc.VitaminUpdate, 0)
	err = ivrsvc.UnmarshalIVRVitaminUpdate(body, &vu)
	if err != nil {
		apiLog.Error("%v", err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"Error": err.Error()})
		return
	}

	for i, el := range vu {
		if el.CurrentlyTakes == true {
			log.Printf("Found a vitamin the user takes. Name: %v, RxCUI: %v", el.SpokenName, el.RxCUI)
			err = ivrsvc.SendVitaminStatment(&vu[i], &pt, ptId)
			if err != nil {
				apiLog.Error("Error sending vitamin update: %v", err.Error())
			}
		}
	}

	c.JSON(http.StatusOK, gin.H{"info": "Ok"})
}

//************************* Local Functions **************************
//********************************************************************

//Retrieves the patient id in string and int format from the URL path
//in the gin.Context.  Will respond to the HTTP request on error.
func getPatientIdFromPath(c *gin.Context) (string, int, error) {
	ptIdStr := c.Param("ptid")
	ptId, err := strconv.Atoi(ptIdStr)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"Error": "The requested patient id could not be found."})
		return "", 0, errors.New("The requested patient id could not be found.")
	}

	return ptIdStr, ptId, nil
}

//For a given id number, returns the patient object.  Will respond to the HTTP request on error.
func getPatientForId(id int, c *gin.Context) (ivrsvc.Patient, error) {
	pt, err := ivrsvc.PatientDetail(id)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"Error": "The requested patient id could not be found."})
		return ivrsvc.Patient{}, err
	}

	return pt, nil
}

func dumpPostData(by []byte) {
	var b bytes.Buffer
	b.Write(by)

	apiLog.Debug("IVR Request Body:\n%v\n", b.String())
}
